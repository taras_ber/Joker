import {View} from 'react-native';
import {Timer} from './timer';

const ShowTikTakTomerClownGamw = ({
  isStartGame,
  showTimer,
  startGame,
  time,
}) => {
  return (
    <>
      {isStartGame && showTimer && (
        <View
          style={{
            position: 'absolute',
            bottom: 10,
            left: '50%',
            width: 200,
            transform: [{translateX: -100}],
          }}>
          <Timer handleFinishTimer={startGame} time={time} />
        </View>
      )}
    </>
  );
};
export default ShowTikTakTomerClownGamw;
