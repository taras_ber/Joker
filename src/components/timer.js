import CountDownTimer from 'react-native-countdown-timer-hooks';

export const Timer = ({handleFinishTimer, time}) => {
  return (
    <CountDownTimer
      // ref={refTimer}
      timestamp={time}
      timerCallback={handleFinishTimer}
      containerStyle={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        widths: '70px',
        paddingVertical: 5,
        borderRadius: 30,
        elevation: 5,
        paddingHorizontal: 30,
        width: '100%',
        height: '100%',
        backgroundColor: '#91060f',
      }}
      textStyle={{
        fontSize: 24,
        fontStyle: 'italic',
        fontWeight: 'bold',
        marginTop: 2,
        letterSpacing: 3,
        color: 'yellow',
      }}
    />
  );
};
