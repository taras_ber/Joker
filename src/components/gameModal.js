import {ImageBackground, StyleSheet, View} from 'react-native';
import StarGameClownModal from './startGameModal';

export const GameModal = ({
  modalVisible,
  background,
  time,
  attempt,
  setModalVisible,
}) => {
  return (
    <ImageBackground
      source={background.image}
      style={styles.centeredView}
      resizeMode="cover" // This will cover the entire view
    >
      <View style={styles.container}>
        <StarGameClownModal
          vidumist={modalVisible}
          chas={time}
          sproba={attempt}
          setSxovatuModalky={setModalVisible}
        />
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'pink',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centeredView: {
    width: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent', // This is the background for the overlay
  },
});
