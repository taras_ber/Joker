import {Modal, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const StarGameClownModal = ({vidumist, chas, sproba, setSxovatuModalky}) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={vidumist}
      onRequestClose={() => {
        setSxovatuModalky(false);
      }}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>
            {`Hi sweetie! You must remember all JOKERS in ${chas} seconds`}
          </Text>
          <Text style={styles.modalTextWrong}>
            {`You can only be wrong ${sproba} times!`}
          </Text>
          <TouchableOpacity
            style={styles.button}
            onPress={() => setSxovatuModalky(false)}>
            <Text style={styles.buttonText}>Start Game!</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};
export default StarGameClownModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'pink',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centeredView: {
    width: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent', // This is the background for the overlay
  },
  modalView: {
    paddingVertical: 20,
    width: '90%',
    backgroundColor: '#91060f',
    padding: 5,
    borderRadius: 30,
    elevation: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalText: {
    color: 'yellow',
    fontWeight: 'bold',
    fontSize: 24,
    fontStyle: 'italic',
    marginBottom: 15,
    textAlign: 'center',
  },
  modalTextWrong: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
    fontStyle: 'italic',
    marginBottom: 15,
    textAlign: 'center',
  },
  button: {
    backgroundColor: 'yellow',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
    elevation: 2, // for Android shadow
  },
  buttonText: {
    color: '#91060f',
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'center',
  },
});
