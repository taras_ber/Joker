export const jokerFirst = {
  image: require('../assets/images/levels/joker1.png'),
  correct: true,
};

export const prikolnuiJoker = {
  image: require('../assets/images/levels/joker2.png'),
  correct: true,
};

export const jokerthirdCool = {
  image: require('../assets/images/levels/joker3.png'),
  correct: true,
};

export const beatyJoker = {
  image: require('../assets/images/levels/joker4.png'),
  correct: true,
};

export const sweatyJoker = {
  image: require('../assets/images/levels/joker5.png'),
  correct: true,
};
export const demonJoker = {
  image: require('../assets/images/levels/joker6.png'),
  correct: true,
};
export const magicJoker = {
  image: require('../assets/images/levels/joker7.png'),
  correct: true,
};
export const evilJoker = {
  image: require('../assets/images/levels/joker8.png'),
  correct: true,
};
export const jokerAlmostLast = {
  image: require('../assets/images/levels/joker9.png'),
  correct: true,
};
export const jokerLast = {
  image: require('../assets/images/levels/joker10.png'),
  correct: true,
};

export const batmanNight = {
  image: require('../assets/images/levels/batman1.png'),
  correct: false,
};
export const batmanDay = {
  image: require('../assets/images/levels/batman2.png'),
  correct: false,
};
export const batmanLondon = {
  image: require('../assets/images/levels/batman3.png'),
  correct: false,
};
export const batmanNightLondon = {
  image: require('../assets/images/levels/batman4.png'),
  correct: false,
};
export const batmanEvilDay = {
  image: require('../assets/images/levels/batman5.png'),
  correct: false,
};
export const batmanReallyCool = {
  image: require('../assets/images/levels/batman6.png'),
  correct: false,
};
export const batmanBestStrong = {
  image: require('../assets/images/levels/batman7.png'),
  correct: false,
};
export const batmanEveningKachan = {
  image: require('../assets/images/levels/batman8.png'),
  correct: false,
};

export const policemanFair = {
  image: require('../assets/images/levels/policeman1.png'),
  correct: false,
};
export const policemanNice = {
  image: require('../assets/images/levels/policeman2.png'),
  correct: false,
};
export const policemanFat = {
  image: require('../assets/images/levels/policeman3.png'),
  correct: false,
};
export const policemanSmile = {
  image: require('../assets/images/levels/policeman4.png'),
  correct: false,
};
export const policemanDiscipline = {
  image: require('../assets/images/levels/policeman5.png'),
  correct: false,
};
export const citizenSimpleMan = {
  image: require('../assets/images/levels/citizen1.png'),
  correct: false,
};
export const citizenInHouse = {
  image: require('../assets/images/levels/citizen2.png'),
  correct: false,
};
export const citizenCleverMan = {
  image: require('../assets/images/levels/citizen3.png'),
  correct: false,
};
export const citizenBeatyRuzha = {
  image: require('../assets/images/levels/citizen4.png'),
  correct: false,
};
export const citizenBrynetka = {
  image: require('../assets/images/levels/citizen5.png'),
  correct: false,
};
export const citizenWomanInBar = {
  image: require('../assets/images/levels/citizen6.png'),
  correct: false,
};
export const circusInside = {
  image: require('../assets/images/levels/circus1.png'),
};
export const circusBigClown = {
  image: require('../assets/images/levels/circus2.png'),
};
export const circusManySharikiv = {
  image: require('../assets/images/levels/circus3.png'),
};
export const circusBrightly = {
  image: require('../assets/images/levels/circus4.png'),
};
