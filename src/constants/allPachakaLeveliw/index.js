import {notVeryCoolLevel1} from '../ysiLevelu/notVeryCollLevel1';
import {secondAlsoWeakLevel} from '../ysiLevelu/secondAlsoWeakLevel';
import {hardEasyLevel} from '../ysiLevelu/HardEasyLevel';
import {middleLevel} from '../ysiLevelu/middleLevel';
import {fiveAlreadyNotEasyLevel} from '../ysiLevelu/fiveAlreadyNotEasyLevel';
import {hotClownLevel} from '../ysiLevelu/hotClownLevel';
import {awesomeLevel} from '../ysiLevelu/awesomeLevel';
import {almostLastLevel} from '../ysiLevelu/almostLastLevel';
import {beatyNineLevel} from '../ysiLevelu/beatyNineLevel';
import {bossLastClownLevel} from '../ysiLevelu/bossLastClownLevel';

export const levels = [
  notVeryCoolLevel1,
  secondAlsoWeakLevel,
  hardEasyLevel,
  middleLevel,
  fiveAlreadyNotEasyLevel,
  hotClownLevel,
  awesomeLevel,
  almostLastLevel,
  beatyNineLevel,
  bossLastClownLevel,
];
