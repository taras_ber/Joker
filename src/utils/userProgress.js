import AsyncStorage from '@react-native-async-storage/async-storage';

export const savePassedClownLevelProgress = async level => {
  try {
    await AsyncStorage.setItem('userLevelProgress', level.toString());
  } catch (error) {
    console.error('Error saving level progress:', error);
  }
};

export const getUserLevelProgressGame = async () => {
  try {
    const value = await AsyncStorage.getItem('userLevelProgress');
    if (value !== null) {
      const a = parseInt(value, 10);
      return a;
    }
  } catch (error) {
    console.error('Error retrieving level progress:', error);
  }
  return null; // default return if no value found or error
};
