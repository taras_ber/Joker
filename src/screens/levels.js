import {
  FlatList,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {levels} from '../constants/allPachakaLeveliw/index';
import {useEffect, useState} from 'react';
import {getUserLevelProgressGame} from '../utils/userProgress';

const LevelButton = ({level, source, passedLevel, navigation}) => {
  const blocked = level > passedLevel + 1;
  return (
    <View style={styles.levelContainer}>
      <TouchableOpacity
        // disabled={level > passedLevel + 1}
        style={styles.levelButton}
        onPress={() => navigation.navigate('PerfectClownGame', {level})}>
        <Image
          source={source.image}
          style={styles.backgroundImage}
          resizeMode="cover"
        />
        {blocked && <View style={styles.disabledOverlay} />}
      </TouchableOpacity>
      <Text style={styles.levelText}>{`Level ${level}`}</Text>
    </View>
  );
};

export const Levels = ({navigation, route}) => {
  const passed = route?.params?.passed;

  const [level, setLevel] = useState(null);
  console.log(level, 'level');
  useEffect(() => {
    const fetchLevelProgress = async () => {
      const retrievedLevel = await getUserLevelProgressGame();
      setLevel(retrievedLevel);
    };

    fetchLevelProgress();
  }, [passed]);
  return (
    <ImageBackground
      source={require('../assets/images/levelBackground.png')}
      style={{
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
      }}>
      <TouchableOpacity
        onPress={() => navigation.navigate('MainClownGameScreen')}
        style={{height: 50, marginLeft: 30, width: 50}}>
        <Image source={require('../assets/images/goback.png')} />
      </TouchableOpacity>
      <View style={styles.container}>
        <FlatList
          scrollEnabled={true}
          data={levels}
          renderItem={({item}) => (
            <LevelButton
              source={item.level}
              passedLevel={level}
              navigation={navigation}
              level={item.id}
            />
          )}
          keyExtractor={item => item.id.toString()}
          numColumns={3}
          columnWrapperStyle={{justifyContent: 'space-between'}}
        />
      </View>
    </ImageBackground>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  levelContainer: {
    alignItems: 'center',
    marginHorizontal: 14,
    marginBottom: 40,
    marginTop: 20,
  },
  levelButton: {
    width: 90, // Match this with backgroundImage
    height: 90, // Match this with backgroundImage
    borderRadius: 45, // Half of width/height
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    overflow: 'hidden',
  },
  disabledOverlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.7)', // Darker semi-transparent black
    borderRadius: 45,
  },
  levelText: {
    fontSize: 22,
    color: 'white',
    // fontWeight: 'bold',
    fontStyle: 'italic',
    marginTop: 1,
  },
  backgroundImage: {
    width: 90, // Match this with levelButton
    height: 90, // Match this with levelButton
    borderRadius: 45, // Half of width/height
    opacity: 1,
  },
});
