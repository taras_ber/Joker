import {levels} from '../constants/allPachakaLeveliw/index';
import {shuffleAllGameCardsToMakeItHarder} from '../utils';
import {useEffect, useMemo, useState} from 'react';
import {GameModal} from '../components/gameModal';
import {FlatList, ImageBackground} from 'react-native';
import Card from '../components/card';
import {SuccessModal} from '../components/successModal';
import {LooseModal} from '../components/loosModal';
import {
  getUserLevelProgressGame,
  savePassedClownLevelProgress,
} from '../utils/userProgress';
import ShowTikTakTomerClownGamw from '../components/showTimerComponent';

export const Game = ({route, navigation}) => {
  const {level} = route.params;
  const {
    images,
    background,
    time,
    column,
    id,
    correctVariant,
    wrongAttemptAmount,
  } = levels.find(l => l.id === level);

  const [isStartGame, setStartGame] = useState(false);
  const [cardDisabled, setCardDisabled] = useState(true);
  const [successModalVisible, setSuccessModalVisible] = useState(false);
  const [looserModalVisible, setLooserModalVisible] = useState(false);
  const [wrongAttempt, setWrongAttempt] = useState(0);
  const [correctAttempt, setCorrectAttempt] = useState(0);
  const [modalVisible, setModalVisible] = useState(true);
  const [showTimer, setShowTimer] = useState(false);
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [showLooserModal, setShowLooserModal] = useState(false);
  const [shouldFlipAll, setShouldFlipAll] = useState(false);
  const goOnLevelScreenSuccess = async () => {
    const currentLevel = await getUserLevelProgressGame();

    if (currentLevel < id) {
      await savePassedClownLevelProgress(id);
    }

    setSuccessModalVisible(false);
    navigation.navigate('TerribleTenLevels', {passed: Date.now()});
  };

  const goOnLevelScreenLooser = () => {
    setLooserModalVisible(false);
    navigation.navigate('TerribleTenLevels', {passed: Date.now()});
  };

  useEffect(() => {
    if (correctAttempt === correctVariant) {
      setShowSuccessModal(true);
      setSuccessModalVisible(true);
    }
  }, [correctAttempt, correctVariant]);

  useEffect(() => {
    if (wrongAttempt === wrongAttemptAmount) {
      setShowLooserModal(true);
      setLooserModalVisible(true);
    }
  }, [wrongAttempt, wrongAttemptAmount]);

  const startTikTakTimerGame = () => {
    setModalVisible(false);
    setStartGame(true);
    setShowTimer(true);
  };

  const startThisSuperDifficultGameGame = () => {
    setShowTimer(false);
    setStartGame(true);
    setShouldFlipAll(true);
    setCardDisabled(false);
  };

  const randomizedPictures = useMemo(
    () => shuffleAllGameCardsToMakeItHarder(images),
    [images],
  );

  return (
    <>
      {modalVisible && (
        <GameModal
          background={background}
          attempt={wrongAttemptAmount}
          modalVisible={modalVisible}
          time={time}
          setModalVisible={startTikTakTimerGame}
        />
      )}

      {isStartGame && (
        <ImageBackground
          source={background.image}
          style={{
            flex: 9, // Taking up 90% of the parent container
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {showSuccessModal && (
            <SuccessModal
              successModalVisible={successModalVisible}
              goOnLevelScreenSuccess={goOnLevelScreenSuccess}
            />
          )}
          {showLooserModal && (
            <LooseModal
              looserModalVisible={looserModalVisible}
              goOnLevelScreenLooser={goOnLevelScreenLooser}
            />
          )}
          <FlatList
            contentContainerStyle={{
              flex: 1,
              marginTop: -50,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            data={randomizedPictures}
            renderItem={({item: carto4ka}) => (
              <Card
                zablokovanaKartochka={cardDisabled}
                flipAllClownCard={shouldFlipAll}
                setWrongAttempt={setWrongAttempt}
                setCorrectAttempt={setCorrectAttempt}
                colonkaCount={column}
                clownskaCartochka={carto4ka}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            numColumns={column}
          />
        </ImageBackground>
      )}
      <ShowTikTakTomerClownGamw
        isStartGame={isStartGame}
        showTimer={showTimer}
        startGame={startThisSuperDifficultGameGame}
        time={time}
      />
    </>
  );
};
