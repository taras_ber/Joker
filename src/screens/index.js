import {MainScreen} from './main';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Levels} from './levels';
import {Game} from './game';

export const AllStack = createNativeStackNavigator();

export const AllScreens = () => {
  return (
    <>
      <AllStack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName={'MainClownGameScreen'}>
        <AllStack.Screen name="MainClownGameScreen" component={MainScreen} />
        <AllStack.Screen name="TerribleTenLevels" component={Levels} />
        <AllStack.Screen name="PerfectClownGame" component={Game} />
      </AllStack.Navigator>
    </>
  );
};
